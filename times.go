package humanize

import (
	"fmt"
	"math"
	"sort"
	"time"
)

// Seconds-based time units
const (
	Day      = 24 * time.Hour
	Week     = 7 * Day
	Month    = 30 * Day
	Year     = 12 * Month
	LongTime = 37 * Year
)

// Time formats a time into a relative string.
//
// Time(someT) -> "3 weeks ago"
func Time(then time.Time) string {
	return RelTime(then, time.Now(), "之前", "之后") // "ago", "from now"
}

// A RelTimeMagnitude struct contains a relative time point at which
// the relative format of time will switch to a new format string.  A
// slice of these in ascending order by their "D" field is passed to
// CustomRelTime to format durations.
//
// The Format field is a string that may contain a "%s" which will be
// replaced with the appropriate signed label (e.g. "ago" or "from
// now") and a "%d" that will be replaced by the quantity.
//
// The DivBy field is the amount of time the time difference must be
// divided by in order to display correctly.
//
// e.g. if D is 2*time.Minute and you want to display "%d minutes %s"
// DivBy should be time.Minute so whatever the duration is will be
// expressed in minutes.
type RelTimeMagnitude struct {
	D      time.Duration
	Format string
	DivBy  time.Duration
}

var defaultMagnitudes = []RelTimeMagnitude{
	{time.Second, "现在", time.Second},      // "now"
	{2 * time.Second, "1 秒 %s", 1},        // "1 second %s"
	{time.Minute, "%d 秒 %s", time.Second}, // "%d seconds %s"
	{2 * time.Minute, "1 分钟 %s", 1},       // "1 minute %s"
	{time.Hour, "%d 分钟 %s", time.Minute},  // "%d minutes %s"
	{2 * time.Hour, "1 小时 %s", 1},         // "1 hour %s"
	{Day, "%d 小时 %s", time.Hour},          // "%d hours %s"
	{2 * Day, "1 天 %s", 1},                // "1 day %s"
	{Week, "%d 天 %s", Day},                // "%d days %s"
	{2 * Week, "1 周 %s", 1},               // "1 week %s"
	{Month, "%d 周 %s", Week},              // "%d weeks %s"
	{2 * Month, "1 个月 %s", 1},             // "1 month %s"
	{Year, "%d 个月 %s", Month},             // "%d months %s"
	{18 * Month, "1 年 %s", 1},             // "1 year %s"
	{2 * Year, "2 年 %s", 1},               // "2 years %s"
	{LongTime, "%d 年 %s", Year},           // "%d years %s"
	{math.MaxInt64, "很久以前 %s", 1},         // "a long while %s"
}

// RelTime formats a time into a relative string.
//
// It takes two times and two labels.  In addition to the generic time
// delta string (e.g. 5 minutes), the labels are used applied so that
// the label corresponding to the smaller time is applied.
//
// RelTime(timeInPast, timeInFuture, "earlier", "later") -> "3 weeks earlier"
func RelTime(a, b time.Time, albl, blbl string) string {
	return CustomRelTime(a, b, albl, blbl, defaultMagnitudes)
}

// CustomRelTime formats a time into a relative string.
//
// It takes two times two labels and a table of relative time formats.
// In addition to the generic time delta string (e.g. 5 minutes), the
// labels are used applied so that the label corresponding to the
// smaller time is applied.
func CustomRelTime(a, b time.Time, albl, blbl string, magnitudes []RelTimeMagnitude) string {
	lbl := albl
	diff := b.Sub(a)

	if a.After(b) {
		lbl = blbl
		diff = a.Sub(b)
	}

	n := sort.Search(len(magnitudes), func(i int) bool {
		return magnitudes[i].D > diff
	})

	if n >= len(magnitudes) {
		n = len(magnitudes) - 1
	}
	mag := magnitudes[n]
	args := []interface{}{}
	escaped := false
	for _, ch := range mag.Format {
		if escaped {
			switch ch {
			case 's':
				args = append(args, lbl)
			case 'd':
				args = append(args, diff/mag.DivBy)
			}
			escaped = false
		} else {
			escaped = ch == '%'
		}
	}
	return fmt.Sprintf(mag.Format, args...)
}
